---
title: Software
layout: page
permalink: /consortium/activities/software/
---
The Consortium can fund software development activities that benefit the open source project.  

We are currently funding **ns-3 wifi module maintenance** with a contract to maintainer Sebastien Deronne.  The work is proceeding in two phases, and the first report is published below.

  * **Phase 1** (through May 2020): **[PHY abstraction, Bianchi example, and rate controls](https://www.nsnam.org/wp-content/uploads/2020/ns-3-wifi-module-upgrade-phase-1.pdf)**  
  * **Phase 2** (in progress): LAA-Wifi coexistence and PHY error model work  

We are also funding **[website development and content migration](https://finance.uw.edu/c2/design-web/getting-started)** for [the main web site](https://www.nsnam.org).

The previously organized ns-3 Consortium (which ran from 2012-2018) funded the following.

  * The port of [ns-3 for Visual Studio 2012](http://www.nsnam.org/wiki/Ns-3_on_Visual_Studio_2012) was organized by the Consortium in 2013.

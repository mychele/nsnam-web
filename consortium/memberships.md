---
title: Memberships
layout: page
permalink: /consortium/memberships/
---
Consortium Members are of four kinds, with the following annual membership fee structure:
  * Universities, non-profits, FFRDCs: $1,500
  * Very small companies (fewer than 20 employees): $1,500
  * Small companies (20-500 employees): $7,500
  * Large companies (greater than 500 employees): $15,000

Prospective members should read the [NS-3 Consortium Affiliation and Membership Agreement](/docs/consortium/NS3_Consortium_Agreement_Bylaws.020619.pdf) and direct inquiries to the consortium director at <consortium-director@nsnam.org>.  The Consortium financial year starts on July 1 annually; membership fees for partial years will be prorated.


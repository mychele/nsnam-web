---
layout: page
title: Consortium
permalink: /consortium/
---

The University of Washington NS-3 Consortium is a collection of organizations cooperating to support the open source project by organizing activities such as the [ns-3 annual meeting](/research/wns3/) including the Workshop on ns-3 and ns-3 training sessions, by providing a point of contact between industry and ns-3 developers, and by supporting administrative activities and purchases necessary to conduct a large project.

Membership to the Consortium is open to those institutions that sign the membership agreement and pay the annual dues.  The Consortium is overseen by an Advisory Board consisting of all members of the Consortium.  The Consortium is governed by an agreement established by the [University of Washington](https://www.washington.edu/).


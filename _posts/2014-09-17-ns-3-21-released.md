---
id: 2943
title: ns-3.21, DCE-1.4 released
date: 2014-09-17T21:56:43+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2943
permalink: /news/ns-3-21-released/
categories:
  - Events
  - News
---
ns-3.21 was released on 17 September 2014 and DCE-1.4 on the following day. For ns-3, several modules have been extended in this release. The LTE module features support for Frequency Reuse algorithms, and also support for the transport of the S1-U, X2-U and X2-C interfaces over emulated links in real-time emulation mode. The Internet module includes new support for a CoDel queue model and support for TCP Timestamps and Window Scale options. Improvements to the energy modeling of Wifi (sleep mode) and a general energy harvesting model were added. In the network module, a new Packet Socket application and helper were added, and the SimpleNetDevice and channel models extended, to facilitate traffic generation and protocol testing without a dependence on the Internet module. In addition, the [RELEASE_NOTES](http://code.nsnam.org/ns-3.21/file/9a397051f10a/RELEASE_NOTES) list the many bugs fixed and small improvements made.

The Direct Code Execution (DCE) project made a related release, version 1.4. Among the new features are a simple bash example illustrating how to make shell script run in your simulation, and a Dockerfile and image for installation with [Docker](http://www.docker.com). The [RELEASE_NOTES](http://code.nsnam.org/ns-3-dce/file/dce-1.4/RELEASE_NOTES) contains more details.

The next ns-3 release is planned for January 2015.
---
id: 3652
title: WNS3 submission deadline extended
date: 2017-02-02T14:28:27+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3652
permalink: /events/wns3-submission-deadline-extended/
categories:
  - Events
---
The annual Workshop on ns-3, to be held in Porto from June 13-14, 2017, is soliciting paper submissions for a recently extended deadline of 12 February 2017. Please see the [call for papers](https://www.nsnam.org/overview/wns3/wns3-2017/call-for-papers/) for more information.
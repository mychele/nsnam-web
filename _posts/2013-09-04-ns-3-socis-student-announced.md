---
id: 2476
title: ns-3 SOCIS student announced
date: 2013-09-04T04:27:36+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2476
permalink: /news/ns-3-socis-student-announced/
categories:
  - Events
  - News
---
Dizhi Zhou has been selected to participate in the 2013 [European Space Agency Summer of Code in Space (SOCIS)](http://sophia.estec.esa.int/socis2013/). A student at the [University of New Brunswick](http://131.202.240.198/index.php/Main_Page), Dizhi also successfully completed an ns-3 project on [LTE MAC scheduling](https://www.nsnam.org/wiki/GSOC2012LTEScheduling) in the 2012 Google Summer of Code. Dizhi&#8217;s project will focus on an implementation of the DTN bundle protocol. This is the ns-3 project&#8217;s first year of participation in SOCIS.

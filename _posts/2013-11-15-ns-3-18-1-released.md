---
id: 2558
title: ns-3.18.1 released
date: 2013-11-15T19:24:42+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2558
permalink: /news/ns-3-18-1-released/
categories:
  - News
  - ns-3 Releases
---
An update to the ns-3.18 release, numbered ns-3.18.1, has now been posted at <https://www.nsnam.org/release/ns-allinone-3.18.1.tar.bz2>.

ns-3.18.1 adds support for the Clang/LLVM compiler used in Apple Xcode 5.0.1, released with OS X Mavericks, and also in FreeBSD and other distributions. ns-3.18.1 also provides Python API scanning support for recent compilers, and updates support (via the bake build system) for [Direct Code Execution version 1.1](http://www.nsnam.org/overview/projects/direct-code-execution/dce-1-1/), released in September 2013.

The next feature release, ns-3.19, is scheduled for mid-December.
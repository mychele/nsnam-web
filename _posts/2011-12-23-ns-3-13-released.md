---
id: 1543
title: ns-3.13 released
date: 2011-12-23T18:25:23+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1543
permalink: /news/ns-3-13-released/
categories:
  - News
  - ns-3 Releases
---
[ns-3.13](/releases/ns-3.13) is a maintenance-oriented release with an upgrade to the [waf build system](http://code.google.com/p/waf/). The first component from the [2011 ns-3 Summer of Code program](http://www.nsnam.org/wiki/index.php/NSOC2011), an IPv6 address generator, is included in this release. The [spectrum module](http://iptechwiki.cttc.es/Ns3_Spectrum) and TCP models also underwent a significant amount of work during this release cycle, and bug fixes and a full transition to XML based trace file generation were added to [NetAnim](http://www.nsnam.org/wiki/index.php/NetAnim). Full details are available in the [RELEASE_NOTES](http://code.nsnam.org/ns-3.13/raw-file/1c73adeb4e8a/RELEASE_NOTES) and [CHANGES.html](http://code.nsnam.org/ns-3.13/raw-file/1c73adeb4e8a/CHANGES.html) files.
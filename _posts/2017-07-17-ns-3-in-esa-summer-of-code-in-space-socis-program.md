---
id: 3758
title: ns-3 in ESA Summer of Code in Space (SOCIS) program
date: 2017-07-17T07:44:54+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3758
permalink: /news/ns-3-in-esa-summer-of-code-in-space-socis-program/
categories:
  - Events
  - News
---
The ns-3 project has been selected to participate in the 2017 edition of the <a href="http://www.esa.int/ESA" target="_blank">European Space Agency (ESA)</a> Summer of Code in Space (SOCIS) program. This marks the fifth consecutive year that ns-3 has been selected. Our 2017 student is Pasquale Imputato, a PhD student in Information Technology and Electrical Engineering at University of Naples Federico II. Pasquale aims to improve the emulation capabilities of the simulator by the introduction of netmap in ns-3. <a href="http://info.iet.unipi.it/~luigi/netmap/" target="_blank">netmap</a> is a fast packet processing technique that bypasses the host networking stack and allows direct network device access. More information on Pasquale&#8217;s project is found on his <a href="https://www.nsnam.org/wiki/SOCIS2017" target="_blank">wiki page</a>.
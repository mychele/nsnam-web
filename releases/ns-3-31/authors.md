---
title: Authors
layout: page
permalink: /releases/ns-3-31/authors/
---
The following people made source code contributions (commits) during the ns-3.31 development period:

  * Alberto Gallegos Ramonet <ramonet@fc.ritsumei.ac.jp>
  * Alexander Krotov <ilabdsf@gmail.com>
  * Amiraslan Haghrah <amiraslanhaghrah@gmail.com>
  * Aron Wussler <aron@wussler.it>
  * Avakash Bhat <avakash261@gmail.com>
  * Bhaskar Kataria <www.bhaskar.com7@gmail.com>
  * Biljana Bojovic <biljana.bojovic@gmail.com>
  * Davide Magrin <magrin.davide@gmail.com>
  * Deepak Kumaraswamy <deepakkavoor99@gmail.com>
  * Eduardo Abinader <eduardo.abinader@gmail.com>
  * Eduardo Almeida <enmsa@outlook.pt>
  * Gabriel Arrobo <gab.arrobo@gmail.com>
  * Gustavo Rezende <gustavorezendesilva@hotmail.com>
  * hax0kartik (GCI 2019) <>
  * howie (GCI 2019) <>
  * InquisitivePenguin (GCI 2019) <>
  * Jared Dulmage <jared.dulmage@wpli.net>
  * Katerina Koutlia <katerina.koutlia@cttc.es>
  * kr0n0s (GCI 2019) <>
  * La Tuan Minh (GCI 2019) <>
  * Manuel Requena <manuel.requena@cttc.es>
  * Michele Polese <michele.polese@gmail.com>
  * Mohit P. Tahiliani <tahiliani.nitk@gmail.com>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Omer Topal <omer.topal@airties.com>
  * Ouassim Karrakchou <okarr102@uottawa.ca>
  * Parth Pandya <parthpandyappp@gmail.com>
  * Parth Pratim (GCI 2018) <>
  * Pasquale Imputato <p.imputato@gmail.com>
  * Peter D. Barnes, Jr <barnes26@llnl.gov>
  * ra1nst0rm3d <>
  * Rahul Bothra <rrbothra@gmail.com>
  * Rediet <getachew.redieteab@orange.com>
  * Ryan Mast <mast9@llnl.gov>
  * SaiMyGuy (GCI 2019) <>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Shikha Bakshi <shikhabakshi912@gmail.com>
  * Shravya K.S <shravya.ks0@gmail.com>
  * Shrinidhi Anil Varna <shrinidhi99.varna@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Steven Smith <smith84@llnl.gov>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Tommaso Zugno <tommasozugno@gmail.com>
  * Vivek Jain <jain.vivek.anand@gmail.com>
  * Xiuchao Wu <xiuchao@amazon.com>
  * Zhao Wen Chow <shu@mos.ics.keio.ac.jp>
  * Zoraze Ali <zoraze.ali@cttc.es>

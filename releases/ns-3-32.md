---
title: ns-3.32
layout: page
permalink: /releases/ns-3-32/
---
  <p>
    <b>ns-3.32</b> was released on October 7, 2020. ns-3.32 includes the following new features:
  </p>

  <ul>
  <li>
  A new TCP congestion control, <b>TcpLinuxReno</b>, has been added.
  </li>
  <li>
  A model for <b>TCP dynamic pacing</b> based on Linux has been added.
  </li>
  <li>
  The <b>PIE queue disc</b> has been extended with additional features:
     <ul>
     <li> <b>queue delay calculation using timestamp</b> feature (Linux default behavior)
     </li>
     <li> <b>ECN</b> support (Section 5.1 of RFC 8033)
     </li>
     <li> <b>derandomization</b> (Section 5.4 of RFC 8033)
     </li>
     <li> <b>cap drop adjustment</b> Section 5.5 of RFC 8033)
     </li> 
     <li>
      <b>Active/Inactive mode</b>
     </li>
     </ul>
  </li>
  <li>
  Added <b>L4S mode</b> to FqCoDel and CoDel queue discs
  </li>
  <li>
  Added <b>netmap</b> and <b>DPDK</b> emulation device variants
  </li>
  <li>
  Added capability to configure <b>STL pair and containers as attributes</b>
  </li>
  <li>
  Added <b>CartesianToGeographic</b> coordinate conversion capability
  </li>
  <li> 
  Added <b>LollipopCounter</b>, a sequence number counter type 
  </li>
  <li>
  Added <b>6 GHz band</b> support for Wi-Fi 802.11ax
  </li>
  </ul>

  <p>
    The release also includes numerous bug fixes and small improvements, listed in the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.32/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
  </p>
  <p>
      <b>Download:</b> The ns-3.32 release download is available from <a href="/releases/ns-allinone-3.32.tar.bz2">this link</a>.  This download is a source archive that contains some additional tools (bake, netanim, pybindgen) in addition to the ns-3.32 source.  The ns-3 source code by itself can also be checked out of our Git repository by referencing the tag 'ns-3.32'.
  </p>

  <p>
    <b>Documentation:</b> The documentation is available in several formats from <a href="/releases/ns-3-32/documentation">this link</a>.
  </p>

  <ul>
    <li>
      What has changed since ns-3.31? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.32/CHANGES.html"> changes</a> and <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.32/RELEASE_NOTES"> RELEASE_NOTES</a> pages for ns-3.32.
    </li>
    <li>
      A patch to upgrade from ns-3.31 to ns-3.32 can be found <a href="/releases/patches/ns-3.31-to-ns-3.32.patch">here</a>
    </li>
    <li>
      Errata containing any late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
    </li>
  </ul>

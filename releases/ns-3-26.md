---
title: ns-3.26
layout: page
permalink: /releases/ns-3-26/
---
<div>
  <p>
    ns-3.26 was released on 3 October 2016 and features the following significant changes.
  </p>

  <ul>
    <li>
      A new class <b>SpectrumWifiPhy</b> has been introduced that makes use of the Spectrum module. Its functionality and API is currently very similar to that of the YansWifiPhy model, especially because it reuses the same InterferenceHelper and ErrorModel classes for this release (although newer error models are now possible).
    </li>
    <li>
      Several new TCP congestion control variants were introduced, including <b>TCP Vegas, Scalable, Veno, Illinois, Bic, YeAH, and H-TCP</b> congestion control algorithms. <li>
        The traffic control module adds models for <b>FQ-CoDel</b>, <b>PIE</b>, and <b>Byte Queue Limits</b>. Extensions were also made to integrate the Wi-Fi module with the traffic control sublayer. A <b>WifiNetDevice::SelectQueue</b> method has been added to determine the user priority of an MSDU, to allow the traffic control layer to align with QoS-aware queues in the Wifi device. As part of the traffic control work, the API for passing QoS and priority from applications down through the Internet stack has been updated, and the PfifoFast queue disc now classifies packets into bands based on their priority.
      </li>
      <li>
        The Wi-Fi module includes better support for <b>IEEE 802.11e</b> features including TXOP limits, and the API for configuring Wi-Fi channel number, center frequency, and standard has been made more consistent.
      </li>
      <p>
        Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.26/raw-file/46d53abb44ad/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
      </p>

      <ul>
        <li>
          The latest ns-3.26 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.26.tar.bz2">here</a>
        </li>
        <li>
          What has changed since ns-3.25? Consult the <a href="http://code.nsnam.org/ns-3.26/raw-file/46d53abb44ad/CHANGES.html">changes</a> page. <li>
            The documentation is available in several formats from <a href="/releases/ns-3-26/documentation">here</a>.
          </li>
          <li>
            Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
          </li>
          <li>
            A patch to upgrade from the last release (ns-3.25) to ns-3.26 can be found <a href="https://www.nsnam.org/release/patches/ns-3.25-to-ns-3.26.patch">here</a>
          </li>

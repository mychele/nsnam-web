---
title: Authors
layout: page
permalink: /releases/ns-3-32/authors/
---
The following people made source code contributions (commits) during the ns-3.32 development period:

  * Alexander Krotov <krotov@iitp.ru>
  * Anna Poon <poon2@llnl.gov>
  * Apoorva Bhargava <apoorvabhargava13@gmail.com>
  * Bhaskar Kataria <bhaskar.k7920@gmail.com>
  * Deepak Kumaraswamy <deepakkavoor99@gmail.com>
  * Gabriel Arrobo <gab.arrobo@gmail.com>
  * Greg Steinbrecher <grs@fb.com>
  * Harsh Patel <thadodaharsh10@gmail.com>
  * Hrishikesh Hiraskar <hrishihiraskar@gmail.com>
  * Jared Dulmage <jared.dulmage@caliola.com>
  * Jordan Dorham <dorham1@llnl.gov>
  * Kim Ferrari <ferrari2@llnl.gov>
  * Mathew Bielejeski <bielejeski1@llnl.gov>
  * Mohit P. Tahiliani <tahiliani.nitk@gmail.com>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Pasquale Imputato <p.imputato@gmail.com>
  * Peter D. Barnes, Jr <barnes26@llnl.gov>
  * Rediet <getachew.redieteab@orange.com>
  * Ryan Mast <mast9@llnl.gov>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Siddharth Singh <siddharth12375@gmail.com>
  * SolomonAnn <thss15_anyz@163.com>
  * Stefano Avallone <stavallo@unina.it>
  * Steven Smith <smith84@llnl.gov>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Vivek Jain <jain.vivek.anand@gmail.com>
  * Xiuchao Wu <xiuchao@amazon.com>
  * Zoraze Ali <zoraze.ali@cttc.es>

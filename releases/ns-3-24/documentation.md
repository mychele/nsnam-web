---
title: Documentation
layout: page
permalink: /releases/ns-3-24/documentation/
---
Documentation for ns-3.24 and ns-3.24.1 releases are identical.

  * **Tutorial:** an introduction into downloading, setting up, and using builtin models:
      * **English:** [pdf](/docs/release/3.24/tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.24/tutorial/singlehtml/index.html), [html (split page)](/docs/release/3.24/tutorial/html/index.html)
      * **Brazilian Portuguese:** [pdf](/docs/release/3.24/tutorial-pt-br/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.24/tutorial-pt-br/singlehtml/index.html), [html (split page)](/docs/release/3.24/tutorial-pt-br/html/index.html)
  * **Manual:** an in-depth coverage of the architecture and core of ns-3: [pdf](/docs/release/3.24/manual/ns-3-manual.pdf), [html (single page)](/docs/release/3.24/manual/singlehtml/index.html), [html (split page)](/docs/release/3.24/manual/html/index.html)
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: [pdf](/docs/release/3.24/models/ns-3-model-library.pdf), [html (single page)](/docs/release/3.24/models/singlehtml/index.html), [html (split page)](/docs/release/3.24/models/html/index.html)
  * [Release Errata](http://www.nsnam.org/wiki/Ns-3.24-errata)
      * [API Documentation](/docs/release/3.24/doxygen/index.html):&nbsp; Coverage of the C++ APIs using Doxygen.
      * Documentation of the [Bake](http://www.nsnam.org/docs/bake/tutorial/html/index.html) integration tool
          * Documentation of the [Direct Code Execution](http://www.nsnam.org/overview/projects/direct-code-execution/) 1.7 release
              * [API Documentation](/docs/dce/release/1.7/doxygen/index.html):&nbsp; Coverage of the APIs using Doxygen.
              * Manual: [html (single page)](/docs/dce/release/1.7/manual/singlehtml/index.html), [html (split page)](/docs/dce/release/1.7/manual/html/index.html)

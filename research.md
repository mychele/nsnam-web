---
layout: page
title: Research
permalink: /research/
---
ns-3 has been used in thousands of publications to date.  To get a feel for the topics studied, search on the keywords 'ns-3 simulator' at the following:

* [Google Scholar](https://scholar.google.com) 
* [IEEE digital library](https://ieeexplore.ieee.org)
* [ACM digital library](https://dl.acm.org) 

The project organizes an annual meeting, the [Workshop on ns-3](https://www.nsnam.org/research/wns3/wns3-2020/), where advances on ns-3 itself are reported.  This year, it was held virtually.  Plans for 2021 are to hold a meeting hosted by NIST in Gaithersburg, MD, if conditions permit.

In August 2020, the U.S. National Science Foundation (NSF) awarded a community research infrastructure (CRI) program to the University of Washington and the Georgia Institute of Technology (Georgia Tech).  The program, led by Prof. Sumit Roy at UW and Prof. Douglas Blough at Ga. Tech, is a multi-year effort to upgrade the scalability of ns-3 wireless models for next-generation Wi-Fi and 5G mmWave simulations.  Another focus will be to perform additional community outreach and to improve ns-3 usability and educational support.  Advances to ns-3 that result from this program will be posted under this Research page as they become available; in the meantime, please see the <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=2016379">award announcement</a>.  

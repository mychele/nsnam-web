---
layout: page
title: Accepted Papers
permalink: /research/wns3/wns3-2013/accepted-papers/
---
  * **An XG-PON Module for the NS-3 Network Simulator**

    Xiuchao Wu, Ken Brown, Cormac Sreenan, Pedro Alvarez, Marco Ruffini, Nicola Marchetti, David Payne and Linda Doyle.
  * **DCE Cradle: Simulate Network Protocols with Real Stacks for Better Realism** ([pdf](http://www.nsnam.org/wp-content/uploads/2013/01/WNS3-2013-Presentation-Tazaki.pdf))

    Hajime Tazaki, Frederic Urbani and Thierry Turletti.
  * **Implementation and Validation for LTE Downlink Schedulers**

    Dizhi Zhou, Nicola Baldo and Marco Miozzo.
  * **Integrating ns3 Model Construction, Description, Preprocessing, Execution, and Visualization** ([pdf](http://www.nsnam.org/wp-content/uploads/2013/01/WNS3-2013-Presentation-Barnes.pdf))

    Peter Barnes, Betty Abelev, Eddy Banks, James Brase, David Jefferson, Sergei Nikolaev, Steven Smith and Ron Soltz.
  * **Nano-Sim: simulating electromagnetic-based nanonetworks in the Network Simulator 3** ([pdf](http://www.nsnam.org/wp-content/uploads/2013/01/WNS3-2013-Presentation-Piro.pdf))

    Giuseppe Piro, Luigi Alfredo Grieco, Gennaro Boggia and Pietro Camarda.
  * **Replication of the Bursty Behavior of Indoor WLAN Channels**

    David Gomez Fernandez, Ramón Agüero Calvo, Marta García-Arranz and Luis Muñoz Gutiérrez.
  * **Simulating Large Topologies in ns-3 using BRITE and CUDA Driven Global Routing**

    Brian Swenson and George Riley.
  * **Simulation of 802.11 Radio-over-Fiber Networks using ns-3** ([pptx](http://www.nsnam.org/wp-content/uploads/2013/01/WNS3-2013-Presentation-Deronne.pptx))

    Sebastien Deronne, Veronique Moeyaert and Sebastien Bette.
  * **TCP Westwood Protocol Implementation in ns-3**

    Siddharth Gangadhar, Truc Anh Nguyen, Greeshma Umapathi and James Sterbenz.
  * **Transactional Traffic Generator Implementation in ns-3**

    Yufei Cheng, Egemen Çetinkaya and James Sterbenz.

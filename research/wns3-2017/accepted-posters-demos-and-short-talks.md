---
layout: page
title: Accepted Posters, Demos, and Short Talks
permalink: /research/wns3/wns3-2017/accepted-posters-demos-short-talks/
---
Accepted posters and demos will be listed here as they are accepted.

  * Amina &Scaron;ljivo, Dwight Kerkhove, Ingrid Moerman, Eli de Poorter, and Jeroen Hoebke, ["Reliability and Scalability Evaluation with TCP/IP of IEEE 802.11ah Networks"](http://www.nsnam.org/workshops/wns3-2017/posters/Sljivo-Kerkhove-Moerman-Poorter-Hoebke.pdf)
  * Serena Santi, Le Tian, and Jeroen Famaey, ["IEEE 802.11ah Restricted Access Window Energy Consumption Model for ns-3"](http://www.nsnam.org/workshops/wns3-2017/posters/Santi-Tian-Famaey.pdf)
  * Nicola Michailow, Vincent Kotzsch, and Douglas Kim, ["Prototyping of Real-Time 5G Software Defined Networks"](http://www.nsnam.org/workshops/wns3-2017/posters/Michailow-Kotzsch-Kim.pdf)
  * Paulo Silva and Nuno T. Almeida, ["WLAN Dense Scenarios: a CSMA/CA Enhancement to Improve MAC Layer Efficiency"](http://www.nsnam.org/workshops/wns3-2017/posters/Silva-Almeida.pdf)
  * Michele Polese, Marco Giordani, Marco Mezzavilla, Sundeep Rangan, and Michele Zorzi, ["Improved Handover Through Dual Connectivity in 5G mmWave Mobile Networks"](http://www.nsnam.org/workshops/wns3-2017/posters/Polese-Giordani-Mezzavilla-Rangan-Zorzi.pdf)
  * Marco Mezzavilla, Menglei Zhang, Michele Polese, Russell Ford, Sourjya DuŠtta, Sundeep Rangan, and Michele Zorzi, ["End-to-end Simulation of 5G mmWave Networks"](http://www.nsnam.org/workshops/wns3-2017/posters/Mezzavilla-Zhang-Polese-Ford-Dutt--a-Rangan-Zorzi.pdf)

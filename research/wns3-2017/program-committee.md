---
title: Program Committee
layout: page
permalink: /research/wns3/wns3-2017/program-committee/
---
### **General Chair**

  * Manuel Ricardo, INESC TEC and Universidade do Porto, <mricardo@inesctec.pt>

### **Technical Program Committee Co-Chairs**

  * Brian Swenson, Georgia Tech Research Institute, <bpswenson@gmail.com>
  * Eric Gamess, Universidad Central de Venezuela <egamess@gmail.com>

### **Proceedings Chair**

  * Eric Gamess, Universidad Central de Venezuela <egamess@gmail.com>

### **Technical Program Committee Members**

  * Alexander Afanasyev, UCLA
  * Ramón Agüero, University of Cantabria, Spain
  * Andrés Arcia-Moret, University of Cambridge, UK
  * Peter D. Barnes, Lawrence Livermore National Laboratory, USA
  * Sebastien Deronne, Nokia, Belgium
  * David Ediger, Georgia Institute of Technology, USA
  * Lorenza Giupponi, CTTC
  * Tom Henderson, University of Washington, USA
  * Sam Jansen, Starleaf Ltd, United Kingdom
  * Kevin Jin, Illinois Institute of Tech
  * Margaret Loper, Georgia Institute of Technology, USA
  * Marco Mezzavilla, New York University
  * Marco Miozzo, CTTC
  * Carlos Moreno, Universidad Central de Venezuela, Venezuela
  * Tommaso Pecorella, Università di Firenze
  * Jani Puttonen, Magister Solutions Ltd.
  * Manuel Ricardo, INESC Porto
  * George F Riley, Georgia Tech
  * Damien Saucez, INRIA
  * Mohit P. Tahiliani, National Institute of Technology Karnataka India
  * Hajime Tazaki, IIJ Innovation Institute
  * Thierry Turletti, INRIA
  * Joerg Widmer, IMDEA Networks

Other members may be added at a later date.

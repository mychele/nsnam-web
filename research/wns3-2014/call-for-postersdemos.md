---
layout: page
title: Call for Posters/Demos
permalink: /research/wns3/wns3-2014/call-for-posters/
---
WNS3 invites your participation in the workshop with a short presentation about work-in-progress, a poster presentation, and/or a poster presentation. Contingent on the number of submissions we receive for this call, we are planning to organize events in addition to the regular paper track. Our goal is to create more opportunities for discussion in the workshop. The scope for this call is similar to that of the CFP for the papers track &#8211; topics of interest include, but are not limited to:

* new models, devices, protocols and applications for ns-3
* using ns-3 in modern networking research
* comparison with other network simulators and emulators
* speed and scalability issues for ns-3
* multiprocessor and distributed simulation with ns-3, including the use of GPUs
* validation of ns-3 models
* credibility and reproducibility issues for ns-3 simulations
* user experience issues of ns-3
* frameworks for the definition and automation of ns-3 simulations
* post-processing, visualization, and statistical analysis tools for ns-3
* models ported from other simulators to ns-3 and models ported from ns-3 to other simulation environments
* using real code for simulation with ns-3 and using ns-3 code in network applications
* integration of ns-3 with testbeds, emulators, and other simulators or tools
* using ns-3 API from programming languages other than C++ or Python
* porting ns-3 to unsupported platforms
* network emulation with ns-3
* ns-3 documentation issues
* ns-3 community issues
* educational uses of ns-3 (teaching and training)
This part of the WNS3 program welcomes works in progress, new problem statements, ideas for future development of the ns-3 code base and ancillary software that supports and/or extends ns-3 functionality.

### Submission instructions

To propose a work-in-progress presentation, poster, or demonstration, authors should submit a one or two-page extended abstract in PDF format through [EasyChair](https://www.easychair.org/conferences/?conf=wns30). Use the [ACM Conference Proceedings format, Option 2 (tighter ‘Alternate’ style)](http://www.acm.org/sigs/publications/proceedings-templates). The abstract should include the basic idea, the scope, and significance of the presentation.

Authors should send a separate email to the workshop chairs, providing information about any equipment they might request for a demonstration and whether any special arrangements will be needed. Be as specific as possible in describing what you will demonstrate. Please include an estimate of the space, and setup time needed for your demonstration.

Accepted work-in-progress, poster and demo abstracts will be published on the ns-3 web site. At least one author of each accepted demo/poster must register and present at the workshop. Each accepted poster will have a corresponding 2-minute presentation to the plenary to introduce a conversation that can be continued in the poster session.

### Technical Program Co-Chairs

* Peter Barnes (barnes26@llnl.gov
* Felipe Perrone (perrone@bucknell.edu)

### Important Dates

* Submission deadline: on a rolling basis up until April 20, 2014
* Notification of acceptance : Within one week of submission (at the latest)
* Workshop in Atlanta : May 7, 2014

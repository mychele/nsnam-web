---
layout: page
title: WNS3 2018 Program
permalink: /research/wns3/wns3-2018/program/
---


# 13th June (Wednesday)

**09:00 to 10:30 am:  Keynote talk:  Bhaskaran Raman, IIT Bombay**
Smart Classrooms: Technology-Aids for Effective Teaching and Learning

**11:00 am to 12:00 pm:  Visualization Tools** (chair:  Mohit Tahiliani):
- Interactive Web Visualizer for IEEE 802.11ah ns-3 Module
- Live Network Simulation in Julia – Design and Implementation of LiveSim.jl

**12:00 to 2:30 pm: Transmission Control Protocol** (chair:  Natale Patriciello):
- BBR – An Implementation of Bottleneck Bandwidth and Round-trip Time Congestion Control for ns-3
- Proportional Rate Reduction for ns-3 TCP
- Design and Implementation of TCP BBR in ns-3

**1:00 to 2:00 pm:**  Lunch break

**2:30 to 3:30 pm:  5G** (chair:  Manuel Ricardo):
- Implementation and Evaluation of Frequency Division Multiplexing of Numerologies for 5G New Radio in ns-3
- Integration of Carrier Aggregation and Dual Connectivity for the ns-3 mmWave Module

**3:30 to 5:00 pm:  IoT** (chair:  Helder Fontes):
- Extension of the IEEE 802.11ah ns-3 Simulation Module
- A LoRaWAN Module for ns-3: Implementation and Evaluation

**5:00 to 6:00 pm:  Other Topics** (chair:  Tom Henderson):
- Implementation of Epidemic Routing with IP Convergence Layer in ns-3
- Improving the ns-3 TraceBasedPropagationLossModel to Support Multiple Access Wireless Scenarios

# 14th June (Thursday)

**09:00 to 11:00 am:  Poster and demo session**

**11:00-11:30 am:**  Tea/Coffee break

**11:30 am to 1:00 pm: Talk on Virtual Labs by Prof. K. V. Gangadharan**

**1:00 to 2:00 pm:** Lunch break

**2:00 to 3:30 pm: NS-3 Consortium Annual Plenary meeting**

**3:30 to 4:00 pm:** Tea / Coffee break

**4:00 to 6:00 pm: Lab tour of Center for System Design (CSD) at NITK Surathkal**

# 15th June (Friday)

**09:00 to 10:30 am: Talk by Arista Networks**

**10:30 to 11:00 am:** Tea / Coffee break

**11:00 am to 12:30 pm: Talk by Criterion Networks (Rahul Hada):  SDN and Openflow overview**

**12:30 to 2:00 pm:** Lunch break

**2:00 to 4:00 pm: ns-3 educational workshop**

**4:00 to 4:30 pm:** Tea / Coffee break

**4:30 to 6:00 pm: ns-3 educational workshop / development discussions**

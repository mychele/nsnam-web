---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2021/program-committee/
---

# Technical Program Co-Chairs

* Stefano Avallone <stavallo@unina.it>
* Michele Polese <m.polese@northeastern.edu>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# Technical Program Committee

To be assembled.
